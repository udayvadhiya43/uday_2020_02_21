const axios = require('axios')

async function loadAllData() {
  const url = `https://jsonplaceholder.typicode.com/comments`;
  return await axios.get(url).then(items => items.data);
}

let finalLCSArray = [];
let fLCSArray = [];
let sLCSArray = [];

const LCS = {
  storeLCS: function(item)
  {
      finalLCSArray.push(item);
  },

  getFLCS() {
    let length = Math.max(...(finalLCSArray.map(el => el.length)));
    finalLCSArray.forEach(element => {
      if(element.length === length)
      fLCSArray.push(element);
    });
    var item = fLCSArray[Math.floor(Math.random() * fLCSArray.length)];
    return item;
  },

  getSLCS() {
    sLCSArray = finalLCSArray;
    let higestLength = Math.max(...(sLCSArray.map(el => el.length)));
    sLCSArray.forEach(element => {
      if(element.length === higestLength)
        var index  = sLCSArray.indexOf(element);
        if (index > -1) {
          sLCSArray.splice(index, 1);
        }
    });
    let secondHigestLength = Math.max(...(sLCSArray.map(el => el.length)));
    sLCSArray = [];
    finalLCSArray.forEach(element => {
      if(element.length === secondHigestLength)
      sLCSArray.push(element);
    });
    var item = sLCSArray[Math.floor(Math.random() * sLCSArray.length)];
    return item;
  },

  calcCachePos: function (indexes, strings) {
    let factor = 1;
    let pos = 0;
    for (let i = 0; i < indexes.length; i++) {
      pos += indexes[i] * factor;
      factor *= strings[i].length;
    }
    return pos;
  },

  lcsBack: function (strings, indexes, cache) {
    for (let i = 0; i < indexes.length; i++)
      if (indexes[i] == -1)
        return "";
    let match = true;
    for (let j = 1; j < indexes.length; j++) {
      if (strings[0][indexes[0]] != strings[j][indexes[j]]) {
        match = false;
        break;
      }
    }
    if (match) {
      let newIndexes = new Array(indexes.length);
      for (let k = 0; k < indexes.length; k++)
        newIndexes[k] = indexes[k] - 1;
      let result = LCS.lcsBack(strings, newIndexes, cache) + strings[0][indexes[0]];
      cache[LCS.calcCachePos(indexes, strings)] = result;
      this.storeLCS(result);
      return result;
    } else {
      let subStrings = new Array(strings.length);
      for (let i = 0; i < strings.length; i++) {
        if (indexes[i] <= 0)
          subStrings[i] = "";
        else {
          let newIndexes = new Array(indexes.length);
          for (let j = 0; j < indexes.length; j++)
            newIndexes[j] = indexes[j];
          newIndexes[i]--;
          let cachePos = LCS.calcCachePos(newIndexes, strings);
          if (cache[cachePos] == null)
            subStrings[i] = LCS.lcsBack(strings, newIndexes, cache);
          else
            subStrings[i] = cache[cachePos];
        }
      }
      let longestString = "";
      let longestLength = 0;
      for (let i = 0; i < subStrings.length; i++) {
        if (subStrings[i].length > longestLength) {
          longestString = subStrings[i];
          longestLength = longestString.length;
        }
      }
      cache[LCS.calcCachePos(indexes, strings)] = longestString;
      return longestString;
    }
  },
  findLCS: function (strings) {
    if (strings.length == 0)
      return "";
    if (strings.length == 1)
      return strings[0];
    let max = -1;
    let cacheSize = 1;
    for (let i = 0; i < strings.length; i++) {
      cacheSize *= strings[i].length;
      if (strings[i].length > max)
        max = strings[i].length;
    }
    let cache = new Array(cacheSize);
    let indexes = new Array(strings.length);
    for (let x = 0; x < indexes.length; x++)
      indexes[x] = strings[x].length - 1;
    LCS.lcsBack(strings, indexes, cache);
  },
  
  getCommentsArray:function(array, index, size) {
    if (array) {
      let newAry =  [...array];
      let arr = newAry.slice(index, size).map(function(x){return x.body.substring(0,100)});
      return arr;
    } else {
      return new Array();
    }
  }
}

export{
  loadAllData,
  LCS
}